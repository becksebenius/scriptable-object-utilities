using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Linq;

[CustomPropertyDrawer(typeof(ScriptableObjectFieldAttribute))]
public class ScriptableObjectFieldAttributeEditor : PropertyDrawer
{
	
	[NonSerialized] bool typesInitialized = false;
	List<Type> types;
	List<GUIContent> names;
	void InitializeTypes ()
	{
		typesInitialized = true;
		types = new List<Type>();
		names = new List<GUIContent>();
		
		var a = attribute as ScriptableObjectFieldAttribute;
		
		names.Add(new GUIContent("Select Type"));
		foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
		{
			var allTypes = assembly.GetTypes();
			foreach(var type in allTypes)
			{
				if(type.IsAbstract) continue;
				if(a.type.IsAssignableFrom(type))
				{
					types.Add(type);
					names.Add(new GUIContent(type.Name));
				}
			}
		}
	}
	
	float linkSize = 18;
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		if(property.isArray)
		{
			linkSize = 20;
			var target = attribute as ScriptableObjectFieldAttribute;
			if(target.isArrayFoldedOut)
			{
				return property.arraySize * linkSize + linkSize + linkSize + linkSize;
			}
			else
			{
				return linkSize;
			}
		}
		else
		{
			return linkSize;
		}
	}
	
	UnityEngine.Object target;
	ScriptableObjectFieldAttribute link;
	
	public override void OnGUI (Rect position,	SerializedProperty prop, GUIContent label) 
	{
		if(!typesInitialized) InitializeTypes();
		
		target = prop.serializedObject.targetObject;
		link = attribute as ScriptableObjectFieldAttribute;
		
		label.tooltip = link.type.Name;
		
		if(prop.isArray)
		{			
//			GUI.Box(position, GUIContent.none);
			
			if(link.newArraySize == -1) link.newArraySize = prop.arraySize;
			
			Rect r = position;
			r.height = linkSize;
			
			if(Event.current.type == EventType.MouseDown && Event.current.button == 0 && r.Contains(Event.current.mousePosition))
			{
				link.isArrayFoldedOut = !link.isArrayFoldedOut;
			}

			EditorGUI.Foldout(r, link.isArrayFoldedOut, label);
			
			if(link.isArrayFoldedOut)
			{
				r.x += 12;
				r.width -= 12;
				r.y += linkSize;
				
				
				bool isEnterEvent = Event.current.Equals(Event.KeyboardEvent("return"));
				
				string ctrlName = prop.propertyPath + "_elementCount";
				
				GUI.SetNextControlName(ctrlName);
				
				link.newArraySize = Mathf.Max(EditorGUI.IntField(r, "Elements", link.newArraySize), 0);
				
				if(GUI.GetNameOfFocusedControl() != ctrlName
				|| (isEnterEvent))
				{
					while(link.newArraySize < prop.arraySize)	prop.DeleteArrayElementAtIndex(prop.arraySize-1);
					while(prop.arraySize < link.newArraySize)
					{
						prop.InsertArrayElementAtIndex(prop.arraySize);
						var p = prop.GetArrayElementAtIndex(prop.arraySize-1);
						p.objectReferenceValue = null;
					}
				}
				
				for(int i = 0; i < prop.arraySize; i++)
				{				
					r.y += linkSize;
					var p = prop.GetArrayElementAtIndex(i);
					
					bool displayX = true;
					var bh = prop.GetArrayElementAtIndex(i);
					if(bh.objectReferenceValue != null)
					{
						displayX = false;
					}
					
					Rect b = r;
					if(displayX) b.width -= 23;
					
					DisplayProperty(b, p, new GUIContent("Element " + i));
					
					if(displayX)
					{
						b.x = b.x + b.width;
						b.width = 23;
						if(GUI.Button(b, "X"))
						{
							DestroyObject(bh.objectReferenceValue);
							prop.DeleteArrayElementAtIndex(i);
							link.newArraySize = prop.arraySize;
							i--;
						}
					}
					
//					GUIStyle style = new GUIStyle(GUI.skin.button);
//					style.margin = new RectOffset();
//					style.padding = new RectOffset();
//					
//					var s = r;
//					s.x-=10;
//					s.width = 24;
//					s.height = 9;
//					s.y++;
//					
//					if(GUI.Button(s, "^", style))
//					{
//						p.MoveArrayElement(i, i-1);
//					}
//					s.y += 8;
//					if(GUI.Button(s, "v", style))
//					{
//						p.MoveArrayElement(i, i+1);
//					}
				}
				
				r.y += linkSize;
				
				float addButtonSize = 23;
				r.x = r.x + r.width - addButtonSize;
				r.width = addButtonSize;
				if(GUI.Button(r, "+"))
				{
					prop.InsertArrayElementAtIndex(prop.arraySize);
					link.newArraySize = prop.arraySize;
					var p = prop.GetArrayElementAtIndex(prop.arraySize-1);
					p.objectReferenceValue = null;
				}
			}
		}
		else
		{
			position.x+=4;
			position.width-=4;
			DisplayProperty(position, prop, label);
		}
		
		prop.serializedObject.ApplyModifiedProperties();
	}
		
	public void DisplayProperty (Rect position, SerializedProperty prop, GUIContent label)
	{
		if(prop.propertyType != SerializedPropertyType.ObjectReference)
		{
			GUI.Label(position, "Scriptable Object Fields must be applied to an object reference.");
			return;
		}
		if(!prop.editable)
		{
			GUI.Label(position, "Serialized Property is not editable.");
			return;
		}
		
		if(prop.objectReferenceValue || !link.allowModification)
		{
			bool displayClearButton = link.allowModification && !prop.isArray && prop.objectReferenceValue;
			
			float clearButtonWidth = 23;
			if(!displayClearButton) clearButtonWidth = 0;
			
			Rect b = position;
			b.width -= clearButtonWidth;
			
			EditorGUI.ObjectField(b, label, prop.objectReferenceValue, link.type, false);
			
			if(displayClearButton)
			{
				b.x = b.x + b.width;
				b.width = clearButtonWidth;
				if(GUI.Button(b, "X"))
				{
					DestroyObject(prop.objectReferenceValue);
					prop.objectReferenceValue = null;
				}
			}
		}
		else
		{			
			int z = EditorGUI.Popup(position, label, 0, names.ToArray());
			if(z != 0)
			{
				var t = types[z-1];
				var obj = ScriptableObject.CreateInstance(t);
				obj.name = "["+t.Name+"]";
				AssetDatabase.AddObjectToAsset(obj, target);
				prop.objectReferenceValue = obj;
			}
		}
    }
	
	void DestroyObject (UnityEngine.Object obj)
	{
		GameObject.DestroyImmediate(obj, true);
	}
}

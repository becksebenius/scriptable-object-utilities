using UnityEngine;
using System.Collections;
using System;

public class ScriptableObjectFieldAttribute : PropertyAttribute
{
	public Type type;
	public bool allowModification = true;
		
	public bool isArrayFoldedOut = true;
	public int newArraySize = -1;
	
	public ScriptableObjectFieldAttribute (Type type)
	{
		this.type = type;
	}
}
